class Banner < ApplicationRecord
  before_save :set_image_by_params_image
  scope :banner_top, -> { where position: 0 }
  scope :banner_upper_footer, -> { where position: 1 }
  scope :banner_footer, -> { where position: 2 }

  mount_uploader :image, BannerUploader
  attr_accessor :image_data

  def view_count
    self.view_times = view_times + 1
    save
  end

  def click_count
    self.click_times = click_times + 1
    save
  end

  private

  def set_image_by_params_image
    self.image = convert_image_data_to_image(image_data) unless image_data.nil?
  end

  def convert_image_data_to_image(image_data)
    temp_img_file = image_data.tempfile
    img_params = { filename: "#{name}#{File.extname(temp_img_file)}",
                   type: image_data.content_type,
                   tempfile: temp_img_file }
    ActionDispatch::Http::UploadedFile.new(img_params)
  end
end
