class ApplicationController < ActionController::Base
  def render_not_found
    render json: {
      message: 'Record not found',
    }, status: 404
  end
end
