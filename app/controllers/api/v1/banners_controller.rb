class Api::V1::BannersController < ApplicationController
  before_action :set_banner, only: [:show, :update, :destroy]

  def index
    @banners = Banner.all
    render json: @banners, status: :ok
  end

  def show
    render json: @banner
    @banner.view_count
  end

  def show_top
    top_banner = Banner.banner_top.order('RAND()').first
    return render_not_found if top_banner.nil?
    top_banner.view_count
    render json: top_banner
  end

  def show_upper_footer
    upper_footer_banner = Banner.banner_upper_footer.order('RAND()').first
    return render_not_found if upper_footer_banner.nil?
    upper_footer_banner.view_count
    render json: upper_footer_banner
  end

  def show_footer
    footer_banner = Banner.banner_footer.order('RAND()').first
    return render_not_found if footer_banner.nil?
    footer_banner.view_count
    render json: footer_banner
  end

  def destroy
    @banner.destroy
  end

  private

  def set_banner
    @banner = Banner.find_by_id(params[:id])
    return render_not_found if @banner.nil?
  end
end
