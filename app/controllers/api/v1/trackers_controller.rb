class Api::V1::TrackersController < ApplicationController
  def banner_click
    banner = Banner.find_by_id params[:id]
    return render_not_found if banner.nil?
    banner.click_count
    render json: {
      message: 'OK'
    }, status: :ok
  end

  def whatsapp_share
    web = Web.find_by_id params[:id]
    return render_not_found if web.nil?
    web.count_whatsapp
    render json: {
      message: 'OK'
    }, status: :ok
  end
end
