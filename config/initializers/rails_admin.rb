RailsAdmin.config do |config|

  # Theme
  ENV['RAILS_ADMIN_THEME'] = 'rollincode'

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :admin
  end
  config.current_user_method(&:current_admin)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  config.show_gravatar = false

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show

    config.model 'Admin' do
      show do
        field :id do
          label 'ID'
        end
        field :email do
          label 'Email'
        end
        field :last_sign_in_at do
          label 'Last Sign In'
        end
        field :encrypted_password do
          label 'Encrypted Password'
        end
        field :sign_in_count do
          label 'Sign In Count'
        end
        field :current_sign_in_ip do
          label 'Current Sign In IP'
        end
      end  
      list do
        field :id do
          label 'ID'
        end
        field :email do
          label 'Email'
        end
        field :last_sign_in_at do
          label 'Last Sign In'
        end
      end
      edit do
        field :email do
          label 'Email'
          help 'Edit Email'
        end
        field :password do
          label 'Password'
          help 'Edit Password'
        end
      end
    end

    config.model 'Banner' do
      show do
        field :name do
          label 'Name'
        end
        field :image do
          label 'Image'
        end
      end
      list do
        field :name do
          label 'Name'
        end
        field :image do
          label "Image"
        end
        field :position, :enum do
          enum do
            [['Top (300x100)',0],['Upper Footer (300x250)',1],['Footer (300x250)',2]]
          end
          label 'Position'
        end
        field :view_times do
          label 'View Times'
        end
        field :click_times do
          label 'Click Times'
        end
      end
      edit do
        field :name do
          label 'Banner Name'
          help 'Name display on the dashboard'
        end
        field :position, :enum do
          enum do
            [['Top (300x100)', 0], ['Upper Footer (300x250)', 1], ['Footer (300x250)', 2]]
          end
          help 'Position that banner will be place at'
        end
        field :landing_page do
          label 'Landing Page URL'
          help 'Put the landing page url here'
        end
        field :image do # field :image, :carrierwave do
          label 'Image URL'
          help 'Upload and put the banner image url here'
        end
      end
    end

    config.model 'Web' do
      list do
        field :id do
          label 'ID'
        end
        field :name do
          label 'Name'
        end
        field :share_whatsapp do
          label 'Whatsapp Share'
        end
        field :share_facebook do
          label 'Facebook Share'
        end
      end
      edit do
        field :name do
          label 'Name'
          help 'Web Ads Name'
        end
      end
    end
  end
end
