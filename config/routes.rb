Rails.application.routes.draw do
  root to: redirect('/admin')
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :banners do
        collection do
          get :show_top
          get :show_upper_footer
          get :show_footer
        end
      end
      resources :trackers do
        collection do
          get :banner_click
          get :whatsapp_share
        end
      end
    end
  end
end
