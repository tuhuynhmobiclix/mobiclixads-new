class CreateWebs < ActiveRecord::Migration[5.1]
  def change
    create_table :webs do |t|
      t.string :name, null: false, default: ''
      t.integer :share_whatsapp, null: false, default: 0
      t.integer :share_facebook, null: false, default: 0

      t.timestamps
    end
  end
end
