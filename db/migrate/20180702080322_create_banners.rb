class CreateBanners < ActiveRecord::Migration[5.1]
  def change
    create_table :banners do |t|
      t.string :name, null: false, default: ''
      t.integer :position, null: false, default: 0
      t.string :landing_page
      t.string :image, :carrierwave
      t.integer :view_times, null: false, default: 0
      t.integer :click_times, null: false, default: 0

      t.timestamps
    end
  end
end
